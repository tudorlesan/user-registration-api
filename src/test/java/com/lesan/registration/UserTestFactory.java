package com.lesan.registration;

import com.lesan.registration.domain.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class UserTestFactory {

    private static LocalDate testDate = LocalDate.of(1993, 4, 24);
    private static Integer ssn = 123456789;

    public static User getUser() {
        return new User("username", "Pass1234", testDate, ssn);
    }

    public static User getUserWithIdAndRegisterDate(int userId, LocalDate registerDate) {
        User user = getUser();
        user.setUserId(userId);
        user.setRegisterDate(registerDate);
        return user;
    }

    public static User getUserInvalidUsername() {
        return new User("user name", "Pass1234", testDate, ssn);
    }

    public static User getUserInvalidPassword() {
        return new User("username2", "password", testDate, ssn);
    }

    public static List<User> getExcludedUsers() {
        return Arrays.asList(new User("adaLovelace", "Analytical3ngineRulz", LocalDate.of(1815, 12, 10), 85385075),
                new User("alanTuring", "Analytical3ngineRulz", LocalDate.of(1912, 6, 23), 123456789),
                new User("konradZuse", "zeD1", LocalDate.of(1910, 6, 22), 987654321));
    }

}

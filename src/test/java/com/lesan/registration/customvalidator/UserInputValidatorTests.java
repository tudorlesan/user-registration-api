package com.lesan.registration.customvalidator;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserInputValidatorTests {

    @Test
    void assertUsername_ReturnsFalse() {
        assertFalse(UserInputValidator.isValidUsername("user name"));
        assertFalse(UserInputValidator.isValidUsername("username "));
        assertFalse(UserInputValidator.isValidUsername(" username"));
        assertFalse(UserInputValidator.isValidUsername(""));
        assertFalse(UserInputValidator.isValidUsername(null));
    }

    @Test
    void assertUsername_ReturnsTrue() {
        assertTrue(UserInputValidator.isValidUsername("username"));
        assertTrue(UserInputValidator.isValidUsername("user"));
    }

    @Test
    void assertPassword_ReturnsFalse() {
        assertFalse(UserInputValidator.isValidPassword("Abc"));
        assertFalse(UserInputValidator.isValidPassword("abc"));
        assertFalse(UserInputValidator.isValidPassword("abcd"));
        assertFalse(UserInputValidator.isValidPassword("ABCD"));
        assertFalse(UserInputValidator.isValidPassword("abcd1"));
    }

    @Test
    void assertPassword_ReturnsTrue() {
        assertTrue(UserInputValidator.isValidPassword("Abcd1"));
        assertTrue(UserInputValidator.isValidPassword("Ab  cd1efg"));
        assertTrue(UserInputValidator.isValidPassword("Ab*^%cd1e!fg"));
    }
}

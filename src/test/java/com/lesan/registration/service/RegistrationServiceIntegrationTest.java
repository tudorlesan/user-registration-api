package com.lesan.registration.service;


import com.lesan.registration.UserTestFactory;
import com.lesan.registration.domain.User;
import com.lesan.registration.repository.RegistrationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//could be integration test
@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class RegistrationServiceIntegrationTest {

    @Autowired
    private RegistrationRepository repository;

    @Autowired
    private ExclusionService exclusionService;

    private RegistrationService registrationService;

    @BeforeEach
    void setUp() {
        registrationService = new RegistrationService(repository, exclusionService);
    }

    @Test
    void registerUser_ReturnsValid() throws InvalidUserException {
        User user = UserTestFactory.getUser();

        User registeredUser = registrationService.registerUser(user);

        assertEquals(user.getUsername(), registeredUser.getUsername());
        assertEquals(user.getPassword(), registeredUser.getPassword());
        assertEquals(user.getDateOfBirth(), registeredUser.getDateOfBirth());
        assertEquals(user.getSsn(), registeredUser.getSsn());
        assertEquals(LocalDate.now(), registeredUser.getRegisterDate());
        assertNotEquals(0, registeredUser.getUserId());
    }

    @Test
    void registerUser_ReturnsInvalidUserAlreadyRegistered() throws InvalidUserException {
        User user = UserTestFactory.getUser();

        registrationService.registerUser(user);
        assertThrowsInvalidUserException(Collections.singletonList(user), InvalidUserException.USER_ALREADY_REGISTERED);
    }

    @Test
    void registerUser_ReturnsInvalidUsername() {
        List<User> usersList = Arrays.asList(UserTestFactory.getUser(),
                UserTestFactory.getUserInvalidUsername());
        assertThrowsInvalidUserException(usersList, InvalidUserException.USERNAME_NOT_VALID);
    }

    @Test
    void registerUser_ReturnsInvalidPassword() {
        List<User> usersList = Arrays.asList(UserTestFactory.getUser(),
                UserTestFactory.getUserInvalidPassword());
        assertThrowsInvalidUserException(usersList, InvalidUserException.PASSWORD_NOT_VALID);
    }

    @Test
    void registerUser_ReturnsInvalidUserExcluded() {
        List<User> excludedUsers = UserTestFactory.getExcludedUsers();

        for (User user : excludedUsers) {
            assertThrowsInvalidUserException(Collections.singletonList(user), InvalidUserException.EXCLUDED_USER);
        }
    }

    private void assertThrowsInvalidUserException(List<User> userList, String expectedReason) {
        try {
            for (User user : userList)
                registrationService.registerUser(user);
            fail("InvalidUserException was not thrown");
        } catch (InvalidUserException e) {
            assertTrue(e.getMessage().contains(expectedReason));
        }
    }

}

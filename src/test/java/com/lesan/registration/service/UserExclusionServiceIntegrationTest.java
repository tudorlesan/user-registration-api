package com.lesan.registration.service;


import com.lesan.registration.repository.ExclusionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserExclusionServiceIntegrationTest {

    @Autowired
    private ExclusionRepository repository;

    private ExclusionService userExclusionService;

    @BeforeEach
    void setUp() {
        userExclusionService = new UserExclusionService(repository);
    }

    @Test
    void validate_ReturnsTrue() {
        assertTrue(userExclusionService.validate("1992-04-14", "124746205"));
        assertTrue(userExclusionService.validate("1815-12-10", "156396039"));
    }

    @Test
    void validate_ReturnsFalse() {
        assertFalse(userExclusionService.validate("1815-12-10", "85385075"));
        assertFalse(userExclusionService.validate("1912-06-23", "123456789"));
        assertFalse(userExclusionService.validate("1910-06-22", "987654321"));
    }

    @Test
    void validateWithInvalidArguments_ReturnsFalse() {
        assertFalse(userExclusionService.validate("", "156396039"));
        assertFalse(userExclusionService.validate("1815-12-10", ""));
        assertFalse(userExclusionService.validate("1815-12-10", null));
        assertFalse(userExclusionService.validate(null, "156396039"));
    }
}


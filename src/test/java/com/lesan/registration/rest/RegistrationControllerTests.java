package com.lesan.registration.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lesan.registration.RegistrationApplication;
import com.lesan.registration.UserTestFactory;
import com.lesan.registration.domain.User;
import com.lesan.registration.repository.ExclusionRepository;
import com.lesan.registration.service.InvalidUserException;
import com.lesan.registration.service.RegistrationService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RegistrationController.class)
@ContextConfiguration(classes = RegistrationApplication.class,
        initializers = ConfigFileApplicationContextInitializer.class)
class RegistrationControllerTests {

    @Autowired
    private
    MockMvc mockMvc;

    @MockBean
    private ExclusionRepository exclusionRepository;

    @MockBean
    private RegistrationService registrationService;


    @Test
    void registerUser_Successful() throws Exception {
        User user = UserTestFactory.getUserWithIdAndRegisterDate(1, LocalDate.now());
        when(registrationService.registerUser(ArgumentMatchers.any())).thenReturn(user);

        mockMvc.perform(post("/register")
                .content(asJsonString(UserTestFactory.getUser()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.userId").exists())
                .andExpect(jsonPath("$.registerDate").exists());
    }

    @Test
    void registerUser_UnSuccessful() throws Exception {
        when(registrationService.registerUser(ArgumentMatchers.any())).thenThrow(new InvalidUserException());

        mockMvc.perform(post("/register")
                .content(asJsonString(UserTestFactory.getUser()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.userId").doesNotExist())
                .andExpect(jsonPath("$.registerDate").doesNotExist());
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

package com.lesan.registration.service;


import com.lesan.registration.customvalidator.UserInputValidator;
import com.lesan.registration.domain.User;
import com.lesan.registration.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

    private ExclusionService exclusionService;

    private RegistrationRepository registrationRepository;

    @Autowired
    public RegistrationService(RegistrationRepository registrationRepository, ExclusionService exclusionService) {
        this.registrationRepository = registrationRepository;
        this.exclusionService = exclusionService;
    }


    public User registerUser(User user) throws InvalidUserException {
        validateUser(user);
        return registrationRepository.save(user);
    }

    public User getUser(String username) throws UsernameNotFoundException {
        User user = registrationRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException();
        }
        return user;
    }

    private void validateUser(User user) throws InvalidUserException {
        if (registrationRepository.findByUsername(user.getUsername()) != null) {
            throw new InvalidUserException(InvalidUserException.USER_ALREADY_REGISTERED);
        }

        if (!UserInputValidator.isValidUsername(user.getUsername())) {
            throw new InvalidUserException(InvalidUserException.USERNAME_NOT_VALID);
        }

        if (!UserInputValidator.isValidPassword(user.getPassword())) {
            throw new InvalidUserException(InvalidUserException.PASSWORD_NOT_VALID);
        }

        if (!exclusionService.validate(user.getDateOfBirth().toString(), String.valueOf(user.getSsn()))) {
            throw new InvalidUserException(InvalidUserException.EXCLUDED_USER);
        }
    }
}

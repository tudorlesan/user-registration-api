package com.lesan.registration.service;

import com.lesan.registration.repository.ExclusionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserExclusionService implements ExclusionService {

    private ExclusionRepository repository;

    @Autowired
    public UserExclusionService(ExclusionRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean validate(String dateOfBirth, String ssn) {
        if (dateOfBirth == null || dateOfBirth.isEmpty() || ssn == null || ssn.isEmpty()) {
            return false;
        }
        return !repository.findByDateOfBirthAndSsn(dateOfBirth, ssn).isPresent();
    }

}

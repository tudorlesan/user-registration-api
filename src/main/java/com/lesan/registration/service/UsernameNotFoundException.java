package com.lesan.registration.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UsernameNotFoundException extends Exception {

    private static final String USER_NOT_FOUND = "Username is not registered.";

    UsernameNotFoundException() {
        super(USER_NOT_FOUND);
    }
}

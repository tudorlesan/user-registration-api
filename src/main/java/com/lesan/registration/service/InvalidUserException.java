package com.lesan.registration.service;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidUserException extends Exception {

    static final String PASSWORD_NOT_VALID = "Password must be at least four characters long, and should contain at " +
            "least one lowercase character, one uppercase character and one number.";
    static final String EXCLUDED_USER = "User is on the exclusion list.";
    static final String USERNAME_NOT_VALID = "A username should be alphanumeric and not contain spaces.";
    static final String USER_ALREADY_REGISTERED = "A user with that username is already registered.";
    private static final String USER_INPUT_ERROR = "Failed to register user: ";

    public InvalidUserException() {
        super("An exception occurred while trying to register a user.");
    }

    InvalidUserException(String reason) {
        super(USER_INPUT_ERROR + reason);
    }
}

package com.lesan.registration.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(ExclusionCriterionId.class)
public class ExclusionCriterion implements Serializable {

    @Id
    private String ssn;
    @Id
    private String dateOfBirth;

    public ExclusionCriterion() {
    }

    public ExclusionCriterion(String ssn, String dateOfBirth) {
        this.ssn = ssn;
        this.dateOfBirth = dateOfBirth;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExclusionCriterion that = (ExclusionCriterion) o;
        return Objects.equals(ssn, that.ssn) &&
                Objects.equals(dateOfBirth, that.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ssn, dateOfBirth);
    }

    @Override
    public String toString() {
        return "ExclusionCriterion{" +
                "ssn=" + ssn +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}

package com.lesan.registration.domain;

import java.time.LocalDate;

public class RegistrationResponse {

    private long userId;
    private LocalDate registerDate;

    public RegistrationResponse(long userId, LocalDate registerDate) {
        this.userId = userId;
        this.registerDate = registerDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }
}

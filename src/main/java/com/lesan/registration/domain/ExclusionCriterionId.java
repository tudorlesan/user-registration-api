package com.lesan.registration.domain;

import java.io.Serializable;

public class ExclusionCriterionId implements Serializable {
    private String ssn;
    private String dateOfBirth;

    public ExclusionCriterionId() {
    }

    public ExclusionCriterionId(String ssn, String dateOfBirth) {
        this.ssn = ssn;
        this.dateOfBirth = dateOfBirth;
    }
}

package com.lesan.registration.repository;

import com.lesan.registration.domain.ExclusionCriterion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExclusionRepository extends JpaRepository<ExclusionCriterion, String> {
    Optional<ExclusionCriterion> findByDateOfBirthAndSsn(String dateOfBirth, String ssn);
}

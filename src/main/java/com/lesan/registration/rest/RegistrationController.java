package com.lesan.registration.rest;


import com.lesan.registration.domain.RegistrationResponse;
import com.lesan.registration.domain.User;
import com.lesan.registration.service.InvalidUserException;
import com.lesan.registration.service.RegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
public class RegistrationController {

    private Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    private RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<?> register(@RequestBody User user) throws InvalidUserException {
        try {
            User registeredUser = registrationService.registerUser(user);
            LOGGER.debug("User with username [" + user.getUsername() + "] was successfully registered");
            return new ResponseEntity<>(new RegistrationResponse(registeredUser.getUserId(), registeredUser.getRegisterDate()), HttpStatus.ACCEPTED);
        } catch (InvalidUserException e) {
            LOGGER.info(e.getMessage());
            throw e;
        }
    }
}

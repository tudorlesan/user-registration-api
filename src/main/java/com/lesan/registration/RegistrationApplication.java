package com.lesan.registration;

import com.lesan.registration.domain.ExclusionCriterion;
import com.lesan.registration.repository.ExclusionRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class RegistrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegistrationApplication.class, args);
    }

    @Bean
    public CommandLineRunner initialExclusionList(ExclusionRepository repo) {
        return args -> repo.saveAll(Arrays.asList(new ExclusionCriterion("85385075", "1815-12-10"),
                new ExclusionCriterion("123456789", "1912-06-23"),
                new ExclusionCriterion("987654321", "1910-06-22")));
    }

}

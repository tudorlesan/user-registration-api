package com.lesan.registration.customvalidator;

public class UserInputValidator {

    public static boolean isValidUsername(String username) {
        return (username != null && !username.isEmpty() && !username.contains(" ") && username.chars()
                .allMatch(Character::isLetterOrDigit));
    }

    public static boolean isValidPassword(String password) {
        return password != null && !password.isEmpty() && password.length() >= 4 && password.chars()
                .anyMatch(Character::isLowerCase) && password.chars().anyMatch(Character::isUpperCase);
    }
}

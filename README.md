# Registration API

An API used for registering users

Developed by Tudor Lesan using Java 11 and Spring Boot

## Features
* Register a user
* Field validation for username and password (username - alphanumeric without spaces, password lengh at least 4 and
containing at least one lowercase character, one uppercase character and a digit)
* In-memory storage
* Stand-alone jar (no need for a pre-installed container/server)

## Should be added/improved
* Security: Service should not accept unauthenticated/unauthorized requests (JWT). Encoding the password.
* Tests: Test speed optimization (@DirtiesContext is very expensive and should be replaced).
* Monitoring: Ensure logs contain all needed information. Metrics and health checks (Actuator)
* Miscellaneous: Dev/Prod profiles and corresponding log levels. Swagger integration. Use aspects for cross-cutting concerns (logging). SSN validation
Exclusion insert feature (currently inserted from hardcoded list)
* Possibly others that have skipped my mind...

## Comments
* Implemented a simple validator since using the Spring Custom Validator seemed overkill
* I assumed the user registration to be a nonconflictual transactional operation.
* The exclusion list can potentially be stored in a cache if needed.

## Requirements
* Java 11
* Maven

## How to start the application
Checkout the project from this repository, then run inside the project folder:
```
    sh start.sh
```

## How to use the application
### Register user
The following request registers a user

```
curl --header "Content-Type: application/json" --request POST --data '{"username":"username","password":"Pass1234","dateOfBirth":"1993-05-26","ssn":"123456789"}' http://localhost:8080/register/
```
Request:
```
    Content-Type - Application/json
    POST localhost:8080/register
    {
        "username":"user",
        "password":"Pass1234",
        "dateOfBirth":"1993-05-23",
        "ssn":"123456789"
    }
```
Response:
```
    HTTP 202 Accepted
    {
        "userId": 1,
        "registerDate": "2020-02-04"
    }

    HTTP 400 Bad Request
    {
        "timestamp": "2020-02-08T14:07:14.101+0000",
        "status": 400,
        "error": "Bad Request",
        "message": "Failed to register user: A user with that username is already registered.",
        "path": "/register/"
    }

```